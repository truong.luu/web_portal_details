const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const dotenv = require('dotenv-webpack');
const BUILD_DIR = path.resolve(__dirname, 'src/app/public');
const APP_DIR = path.resolve(__dirname, 'src/app');
require('babel-polyfill');

module.exports = {
    entry: {
        index: ['babel-polyfill', './src/index.js']
    },
    output: {
        path: BUILD_DIR,
        filename: "[name].[chunkhash:4].js"
    },
    module: {
        rules: [
            {
                test: /\.(jsx|js)?$/,
                loader: 'babel-loader'
            },
            {
                test: /\.(png|woff(2)?|eot|ttf|svg)$/,
                include: APP_DIR,
                loader: ['url-loader?limit=100000']
            },
            {
                test: /\.css$/,
                include: APP_DIR,
                loader: ['style-loader', 'css-loader'],
            },
            {
                test: /\.css$/,
                include: /node_modules/,
                loader: ['style-loader', 'css-loader'],
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new dotenv({
            path: './' + (process.env.NODE_ENV ? process.env.NODE_ENV: '') + '.env',
        }),
    ],
    devServer: {
        historyApiFallback: true
    },
};
