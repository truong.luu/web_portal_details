export const URL = {
    GET_ROLE_OPTIONS: `listroles`,
    GET_PREFERENCE_OPTIONS: `listterms`,
    GET_SCHOOL_NAME_BY_SCHOOL_POST_CODE: (schoolPostCode) => `/schoolsbypostcode?postcode=${schoolPostCode}`,
    SUBMIT_FORM: `/submit`
};