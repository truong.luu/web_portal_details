import axios from 'axios';

var divElements = [];
const addLoading = () => {
    let divElement = document.createElement("div");
    divElement.className = "full-mask";
    divElement.innerHTML = ` 
        <div class="loading">
            <img src="/image/rr_loading.gif" height="60px"/>
        </div>
    `;
    if (!divElements.length){
        document.body.appendChild(divElement);
    }
    divElements.unshift(divElement);
};

const removeLoading = () => {
    if (divElements.length === 1) {
        document.body.removeChild(divElements[0]);
    }
    divElements.shift();
};

const apiFetch = () => {
    const defaultOptions = {
        baseURL: process.env.API_PATH,
        headers: {
            'Content-Type': 'application/json',
        },
    };
    let instance = axios.create(defaultOptions);
    instance.interceptors.request.use(function (config) {
        const isLoading = config.hasOwnProperty('isLoading') ? config.isLoading : true;
        if (isLoading) {
            addLoading();
        }
        return config;
    });
    instance.interceptors.response.use(
        function (response) {
            removeLoading();
            return response;
        },
        function (error) {
            removeLoading();
            return Promise.reject(error);
        }
    );

    return instance;
};

export default apiFetch();