import React from "react";
import { isEmail, isMobilePhone, isNumeric } from "validator";

export const required = (value) => {
    if (!value || !value.trim()) {
        return <div className="has-error">This field is required.</div>;
    }
};

export const numeric = (value) => {
    if (value && !isNumeric(value)) {
        return <div className="has-error">Must be a numeric.</div>;
    }
};

export const email = (value) => {
    if (!isEmail(value)) {
        return <span className="has-error">Must be a valid email address.</span>;
    }
};

export const mobilePhone = (value) => {
    if (value && !isMobilePhone(value)) {
        return <span className="has-error">Must be a valid phone number.</span>;
    }
};

export const isEqual = (value, props, components) => {
    const bothUsed = components.password[0].isUsed && components.confirm[0].isUsed;
    const bothChanged = components.password[0].isChanged && components.confirm[0].isChanged;

    if (bothChanged && bothUsed && components.password[0].value !== components.confirm[0].value) {
        return <span className="has-error">Passwords are not equal.</span>;
    }
};

export const checkBoxRequired = (value, props, components) => {
    if(typeof(value)==="object" && value.length <= 0){
        return <div className="has-error"><span className="help-block">This field is required.</span></div>;
    }
    if(value==""){
        return <div className="has-error"><span className="help-block">This field is required.</span></div>;
    }
    if(props.max!=undefined){
        if(typeof(value)==="object" && value.length != props.max){
            return <div className="has-error"><span className="help-block">Please select {props.max} option</span></div>;
        }
    }

};

export const termDifference = (value) => {
    const item = value.filter(item => !!item);
    if ([...new Set(item)].length !== item.length) {
        return <div className="has-error">You can't choose the same term more than once.</div>;
    }
    if (value.findIndex(data => !data) > -1) {
        return <div className="has-error">Preferences field is required.</div>;
    }
};
