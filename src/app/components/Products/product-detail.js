import React from 'react';

class ProductDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        $(document).ready(function () {
            let stopAllYouTubeVideos = () => {
                var iframes = document.querySelectorAll('iframe');
                Array.prototype.forEach.call(iframes, iframe => {
                    iframe.contentWindow.postMessage(JSON.stringify({ event: 'command', func: 'stopVideo' }), '*');
                    iframe.contentWindow.postMessage(JSON.stringify({method: 'pause', value: 'true'}), '*');
                });
            };
            $("#carousel-example").on("slide.bs.carousel", function (args) {
                stopAllYouTubeVideos();
            })
        });
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="top-content">
                    <div className="row">
                        <div className="col col-md-8">
                            <div className="product-detail__title">
                                <span className="title">Orange Shore - Font</span>
                                <svg width="1em" height="1em" viewBox="0 0 16 16" className="pointer bi bi-download float-right"
                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                          d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                    <path fill-rule="evenodd"
                                          d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                </svg>
                            </div>
                            <div id="carousel-example" className="carousel slide">
                                <div className="carousel-inner">
                                    <div className="carousel-item active">
                                        <div className="embed-responsive embed-responsive-16by9">
                                            <iframe className="embed-responsive-item carousel-item__body"
                                                    src="https://www.youtube.com/embed/6hgVihWjK2c?rel=0&enablejsapi=1"
                                                    allowFullScreen></iframe>
                                        </div>
                                    </div>
                                    <div className="carousel-item">
                                        <img src="image/slide-img-1.jpg" className="d-block carousel-item__body"
                                             alt="slide-img-1" />
                                    </div>
                                    <div className="carousel-item">
                                        <div className="embed-responsive embed-responsive-16by9">
                                            <iframe className="embed-responsive-item carousel-item__body"
                                                    src="https://player.vimeo.com/video/84910153?rel=0&enablejsapi=1&title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff"
                                                    allowFullScreen></iframe>
                                        </div>
                                    </div>
                                    <div className="carousel-item">
                                        <img src="image/slide-img-2.jpg" className="d-block carousel-item__body"
                                             alt="slide-img-2" />
                                    </div>
                                    <div className="carousel-item">
                                        <div className="embed-responsive embed-responsive-16by9">
                                            <iframe className="embed-responsive-item carousel-item__body"
                                                    src="https://www.youtube.com/embed/oiKj0Z_Xnjc?rel=0&enablejsapi=1"
                                                    allowFullScreen></iframe>
                                        </div>
                                    </div>
                                    <a className="carousel-control-prev" href="#carousel-example" role="button"
                                       data-slide="prev">
                                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span className="sr-only">Previous</span>
                                    </a>
                                    <a className="carousel-control-next" href="#carousel-example" role="button"
                                       data-slide="next">
                                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span className="sr-only">Next</span>
                                    </a>
                                </div>
                                <div className="thumbnail">
                                    <ol className="carousel-indicators">
                                        <li data-target="#carousel-example" data-slide-to="0" className="active">
                                            <img src="image/slide-img-2.jpg" className="d-block w-100"
                                                 alt="slide-img-2" /></li>
                                        <li data-target="#carousel-example" data-slide-to="1"><img
                                            src="image/slide-img-2.jpg" className="d-block w-100"
                                            alt="slide-img-2" /></li>
                                        <li data-target="#carousel-example" data-slide-to="2"><img
                                            src="image/slide-img-2.jpg" className="d-block w-100"
                                            alt="slide-img-2" /></li>
                                        <li data-target="#carousel-example" data-slide-to="3"><img
                                            src="image/slide-img-2.jpg" className="d-block w-100"
                                            alt="slide-img-2" /></li>
                                        <li data-target="#carousel-example" data-slide-to="4"><img
                                            src="image/slide-img-2.jpg" className="d-block w-100"
                                            alt="slide-img-2" /></li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                        <div className="col-md-4">
                            <button
                                type="button"
                                className="close btn-close"
                                aria-label="Close"
                                onClick={() => this.props.history.push('product-media')}
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <br />
                            <br />
                            <div className="d-flex justify-content-center">Orange Shore - Font</div>
                            <hr className="content-right__title-border"/>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Shot type</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">Front view</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Format</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">PNG</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Dimensions</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">23cm L</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Weight</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">0.8kg</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Size</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">128 KB</div>
                            </div>
                            <hr className="content-right__title-border"/>


                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Category</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">Shoes</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Brand</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">Nike</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Department</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">Athletics</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Keywords</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">Red;Trainers shoe;Size8</div>
                            </div>
                            <hr className="content-right__title-border"/>

                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Created</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">1 Sep 2020; 4:43PM</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Modified</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">1 Sep 2020; 4:43PM</div>
                            </div>
                            <hr className="content-right__title-border"/>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Flagged Reason</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">Product Removal (too much background removed)</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Comment</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">Need to reshoot this product</div>
                            </div>
                            <hr className="content-right__title-border"/>

                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Aperture</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">1.7</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Shutter speed</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">1/500</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">ISO</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">ISO-53</div>
                            </div>
                            <div className="row content-right__title">
                                <div className="pdr-0 col-md-4 col-sm-4">Focal Length</div>
                                <div className="pdr-0 pdl-0 col-md-1 col-sm-1">:</div>
                                <div className="pdr-0 pdl-0 col-md-6 col-sm-6">4.4mm</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductDetail;