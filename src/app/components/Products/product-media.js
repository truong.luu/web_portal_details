import React from 'react';

class ProductMedia extends React.Component {
    constructor(props) {
        super(props);
        this.handelViewAllProduct = this.handelViewAllProduct.bind(this);
    }

    handelViewAllProduct() {
        this.props.history.push('/');
    }

    render() {
        return (
            <div className="container-fluid">
                <br />
                <div className="row">
                    <div className="col-md-11">
                        <span style={{opacity: '0.5'}} className="pointer" onClick={this.handelViewAllProduct}>All Products ></span>
                        <span> Orange Shoes</span>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <p>
                            <b>1 Sep 2020</b>
                            <div className="download-all float-right">
                                <span className="title">Download All</span>
                                <svg width="1em" height="1em" viewBox="0 0 16 16" className="pointer bi bi-download float-right"
                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                          d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                    <path fill-rule="evenodd"
                                          d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                </svg>
                            </div>
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-2 pdr-0">
                        <div className="card">
                            <img
                                className="pointer card-img-top"
                                src="image/rr_loading.gif"
                                alt="Card image cap"
                                onClick={() => this.props.history.push('product-detail')}
                            />
                        </div>
                        <div className="product-name"> 1 Sep 2020</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductMedia;