import React, {createRef} from 'react';
import formatBytes from "../../helpers";

class Products extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isListView: true,
            gridData: {},
            listData: [],
            totalProduct: 0,
        };
        this.handelViewProduct = this.handelViewProduct.bind(this);
    }

    handelViewProduct() {
        this.props.history.push('product-media');
    }

    getProductSelected() {
        let {gridData} = this.state;
        let productsSelected = [];
        for (let key in gridData) {
            const itemSelected = gridData[key].data.filter(item => !!item.checked);
            productsSelected = [...productsSelected, ...itemSelected];
        }
        return productsSelected;
    }

    async getProductData() {
        const [gridDataResponse, listDataResponse] = await Promise.all([
            apiFetch.get('https://ihokwxw9u3.execute-api.ap-southeast-2.amazonaws.com/latest/mmapt-list-api?format=grid'),
            apiFetch.get('https://ihokwxw9u3.execute-api.ap-southeast-2.amazonaws.com/latest/mmapt-list-api?format=list')
        ]);
        let totalProduct = 0;
        let gridData = {};
        for (let key in gridDataResponse.data) {
            gridData[key] = {
                data: gridDataResponse.data[key],
                isShowCheckbox: false,
            };
            totalProduct += gridDataResponse.data[key].length
        }
        this.setState({totalProduct, gridData, listData: listDataResponse.data});
    }

    componentDidMount() {
        this.getProductData();
    }

    render() {
        return (
            <div className="container-fluid">
                <br />
                <div className="row">
                    <div className="col-md-11">{`Total ${this.state.totalProduct} Products`}</div>
                    <span className="pointer btn-group float-right col-md-1">
                        <i onClick={(e) => {
                            this.setState({isListView: true})
                        }} style={{opacity: this.state.isListView ? 1 : 0.4}}
                            className="fa fa-th-large">
                        </i>&nbsp;&nbsp;
                        <i onClick={(e) => {
                            this.setState({isListView: false})
                        }} style={{opacity: !this.state.isListView ? 1 : 0.4}}
                           className="fa fa-list-ul">
                        </i>
                    </span>
                </div>
                <br />
                {this.state.isListView && (
                    <React.Fragment>
                        {this.getProductSelected().length > 0 &&
                        <div className="row">
                            <div className="col-md-3">
                                <div className="product-download">
                                    <span className="title">{`${this.getProductSelected().length} Products selected`}</span>
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="pointer bi bi-download float-right"
                                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                        <path fill-rule="evenodd"
                                              d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        }
                        {Object.keys(this.state.gridData).map(key => {
                            let {gridData} = this.state;
                            const gridDataCurrent = gridData[key];
                            const isShowCheckbox = gridDataCurrent.isShowCheckbox;
                            const option = gridDataCurrent.data.map((item, index) => {
                                return (
                                    <div key={index + item.name} className="col-md-2 pdr-0">
                                        <div className="card">
                                            {isShowCheckbox &&
                                                <input
                                                    type="checkbox"
                                                    className="pointer checkbox-image"
                                                    checked={!!item.checked ? 'checked' : false}
                                                    onClick={e => {
                                                        item.checked = e.target.checked;
                                                        gridData[key].data[index] = item;
                                                        this.setState({gridData});
                                                    }}
                                                />
                                            }
                                            <img onClick={this.handelViewProduct} className="pointer card-img-top" src={item.url} alt="Card image cap" />
                                        </div>
                                        <div className="product-name">{item.name}</div>
                                    </div>
                                );
                            });
                            return (
                                <React.Fragment key={key}>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <p>
                                            {isShowCheckbox &&
                                                <React.Fragment>
                                                    <input
                                                        type="checkbox"
                                                        className="pointer checkbox-all-image"
                                                        checked={
                                                            gridDataCurrent.data.filter(item => !!item.checked).length === gridDataCurrent.data.length ? 'checked' : false
                                                        }
                                                        onClick={(e) => {
                                                            gridData[key].data = gridData[key].data.map(item => {
                                                                item.checked = e.target.checked;

                                                                return item;
                                                            });
                                                            this.setState({gridData});
                                                        }}
                                                    />
                                                    &nbsp;&nbsp;
                                                </React.Fragment>
                                            }
                                            <b
                                                className="pointer"
                                                onClick={() => {
                                                gridData[key].isShowCheckbox = !gridData[key].isShowCheckbox;
                                                gridData[key].data = gridData[key].data.map(item => {
                                                    item.checked = false;

                                                    return item;
                                                });
                                                this.setState({gridData});
                                            }}
                                            >
                                                {key}
                                            </b>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row">
                                        {option}
                                    </div>
                                </React.Fragment>
                            );
                        })}
                    </React.Fragment>
                )}
                {!this.state.isListView &&
                    <table className="table__product-list">
                        <tr className="table-header">
                            <th className="name-column">Name&nbsp;&nbsp;
                                <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-arrow-down"
                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                          d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z"/>
                                </svg>
                            </th>
                            <th>Image</th>
                            <th>Videos</th>
                            <th>Size</th>
                            <th>Category</th>
                            <th>Date Added&nbsp;&nbsp;
                                <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-arrow-down"
                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                          d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z"/>
                                </svg>
                            </th>
                            <th>Actions</th>
                        </tr>
                        {this.state.listData.map(item => {
                            return (
                                <tr>
                                    <td className="text-left">
                                        <div className="">
                                            <img
                                                className="product-image"
                                                src={item.url}
                                                alt="image cap"
                                            />
                                            &nbsp;&nbsp;&nbsp;
                                            <span>{item.name}</span>
                                        </div>
                                    </td>
                                    <td>{item.images}</td>
                                    <td>{item.videos}</td>
                                    <td>{`${formatBytes(item.size)}`}</td>
                                    <td>{item.category}</td>
                                    <td>{item.addedDate}</td>
                                    <td>
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="pointer bi bi-download"
                                             fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                            <path fill-rule="evenodd"
                                                  d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                        </svg>
                                    </td>
                                </tr>
                            )
                    })}
                    </table>
                }
            </div>
        );
    }
}

export default Products;
