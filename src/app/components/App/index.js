import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Products from '../Products'
import ProductDetail from "../Products/product-detail";
import '../../styles/bootstrap.css';
import PageNotFound from "../PageNotFound";
import ProductMedia from "../Products/product-media";

class App extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Products}/>
                    <Route path="/product-media" component={ProductMedia}/>
                    <Route path="/product-detail" component={ProductDetail}/>
                    <Route component={PageNotFound} />
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App;
