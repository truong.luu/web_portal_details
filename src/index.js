import React from 'react';
import { render } from 'react-dom';
import './app/styles/main.css'
import App from './app/components/App';
import apiFetch from './app/helpers/apiFetch';

window.apiFetch = apiFetch;

render(
    <App />, document.getElementById('rr_form')
);
