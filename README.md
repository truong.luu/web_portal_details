#Setup
1. Clone source from this repository
2. Install required npm packages with `yarn install`
3. Start webpack development server with `yarn start`

#Production Build


Production ready `bundle.js` and `style.css` files is generated with `webpack --progress -p` in `src/app/public` folder where index html and images are already present.
